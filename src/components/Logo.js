import React from "react";
import '../css/Logo.css'
import FCCLogo from '../images/logo.svg';

const Logo = () => {
    return <div className='fcc-logo-contenedor'>
        <img
            src={FCCLogo}
            className='fcc-logo'
            alt='logo'
        />
    </div>
}

export default Logo